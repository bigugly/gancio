---
layout: default
title: Install
permalink: /install
has_children: true
nav_order: 3
has_toc: false
---
## Install

You can install gancio on a cheap VPS (500mb of ram will be enough)

- [Install on Debian]({% link install/debian.md %})
- [Install using docker]({% link install/docker.md %})

### Post installation
- [Setup Nginx as a proxy]({% link install/nginx.md %})
- [Configuration]({% link install/configuration.md %})
- [Backup]({% link install/backup.md %})


If you wanna hack or run the current development release take a look at [Hacking & contribute]({% link dev/dev.md %})
