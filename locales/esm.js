export default {
  it: 'Italiano',
  en: 'English',
  es: 'Español',
  ca: 'Català',
  pl: 'Polski',
  eu: 'Euskara',
  nb: 'Norwegian Bokmål',
  fr: 'Francais',
  de: 'Deutsch'
}
