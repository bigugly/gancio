// vuetify.options.js
export default {
  theme: {
    dark: true,
    // theme: { disable: true },
    themes: {
      dark: {
        primary: '#FF6E40'
      },
      light: {
        primary: '#FF4500'
      }
    }
  }
}
